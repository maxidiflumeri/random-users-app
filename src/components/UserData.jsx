import * as React from "react";
import { useContext, useEffect, useState } from "react";
import usersContext from "../context/users";
import UserCard from "./UserCard/UserCard";
import Button from "@mui/material/Button";

export default function UserData() {
  const { getUsers, users } = useContext(usersContext);
  const [usersMap, setUsersMap] = useState([]);
  const [usersCant, setUsersCant] = useState(6);
  const [estaCargando, setEstaCargando] = useState(false);

  useEffect(() => {
    get();
  }, []);

  useEffect(() => {    
    setUsersMap(users.slice(0, usersCant));    
  }, [users]);

  async function get() {
    setEstaCargando(true);
    await getUsers();
    setEstaCargando(false);
  }

  function handleClickUsers() {
    let arrayOrigin = usersMap;
    let arrayAux = users.slice(usersCant, usersCant + 6);
    arrayAux.forEach((element) => {
      arrayOrigin.push(element);
    });
    setUsersMap(arrayOrigin);
    setUsersCant(usersCant + 6);
  }

  if (estaCargando) {
    return (
      <div className="alert alert-warning shadow text-center">
        Cargando usuarios...
      </div>
    );
  } else {
    return (
      <div className="container">
        <div className="row d-flex justify-content-center">
          {usersMap.map((value, index) => (
            <div
              key={index}
              className="mt-2 col-sm-12 col-md-4 col-lg-4 col-xl-4"
            >
              <UserCard user={value}></UserCard>
            </div>
          ))}
        </div>
        <div className="row d-flex justify-content-center">
          <div className="col-4 text-center">
            <Button
              className="mt-3"
              variant="contained"
              onClick={handleClickUsers}
            >
              Cargar mas...
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
