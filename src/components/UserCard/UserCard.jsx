import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import UserDetailsDialog from "../UserDetailsDialog/UserDetailsDialog";
import Dialog from "@mui/material/Dialog";

export default function UserCard({ user }) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClickClose = () => {
      setOpen(false)
  }  

  return (
    <>
      <Card sx={{ maxWidth: 350 }}>
        <CardActionArea onClick={handleClickOpen}>
          <CardMedia
            component="img"
            height="250"
            image={user.picture.large}
            alt="green iguana"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
            {user.name.title}. {user.name.first} {user.name.last}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              <strong>Celular:</strong> {user.cell}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              <strong>Correo:</strong> {user.email}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              <strong>Edad:</strong> {user.dob.age}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              <strong>Fecha Nacimiento:</strong>{" "}
              {user.dob.date.substring(0, 10)}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
      <Dialog maxWidth="md" open={open}>
        <UserDetailsDialog onDialogClose={handleClickClose} user={user}></UserDetailsDialog>
      </Dialog>
    </>
  );
}
