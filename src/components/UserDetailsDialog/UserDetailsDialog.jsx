import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

export default function UserDetailsDialog(props) {
  const { user, onDialogClose } = props;

  const handleClose = () => {
    onDialogClose();
  };  

  return (
    <>
      <DialogTitle>
        {user.name.first} {user.name.last}
      </DialogTitle>
      <DialogContent>
        <TextField
          className="mx-1 my-1"
          label="Correo Electrónico"
          type="email"
          variant="standard"
          value={user.email}
          disabled
        />

        <TextField
          className="mx-1 my-1"
          label="Fecha de Nacimiento"
          type="text"
          variant="standard"
          value={user.dob.date.substring(0, 10)}
          disabled
        />

        <TextField
          className="mx-1 my-1"
          label="Telefono Particular"
          type="text"
          variant="standard"
          value={user.phone}
          disabled
        />
        <TextField
          className="mx-1 my-1"
          label="Telefono Celular"
          type="text"
          variant="standard"
          value={user.cell}
          disabled
        />
        <TextField
          className="mx-1 my-1"
          label="Sexo"
          type="text"
          variant="standard"
          value={user.gender}
          disabled
        />
        <TextField
          className="mx-1 my-1"
          label="Direccion"
          type="text"
          variant="standard"
          value={user.location.street.name + " " + user.location.street.number}
          disabled
        />
        <TextField
          className="mx-1 my-1"
          label="Ciudad"
          type="text"
          variant="standard"
          value={user.location.city}
          disabled
        />
        <TextField
          className="mx-1 my-1"
          label="Provincia"
          type="text"
          variant="standard"
          value={user.location.state}
          disabled
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancelar</Button>
      </DialogActions>
    </>
  );
}
