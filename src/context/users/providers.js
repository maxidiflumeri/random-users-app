import UsersContext from "./index"
import getAll from "../../services/apis/user.service"
import { useState } from "react"

export default function UserProvider({ children }) {
    const [users, setUsers] = useState([]);    

    const getUsers = async () =>{        
        const usersAux = await getAll();
        setUsers(usersAux)        
    }

    return (
        <UsersContext.Provider value={{ getUsers, users}}>
            {children}
        </UsersContext.Provider>
    )


}