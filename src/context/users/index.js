import { createContext } from "react";

const usersContext = createContext('users')

export default usersContext