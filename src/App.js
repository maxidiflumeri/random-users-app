import UserData from "./components/UserData";
import UserProvider from "./context/users/providers";

function App() {
  return (
    <UserProvider>
      <div className="container mt-3">
        <UserData></UserData>
      </div>
    </UserProvider>
  );
}

export default App;
