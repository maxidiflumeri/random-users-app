import axios from 'axios'

export default async function getAll(){
    try{
        const response = await axios.get('https://randomuser.me/api/?results=100')                
        return response.data.results
    }catch(error){
        console.log(error)
    }

}