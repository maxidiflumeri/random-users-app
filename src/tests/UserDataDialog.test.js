import React from 'react'
import '@testing-library/jest-dom'
import { screen, render } from '@testing-library/react'
import UserDataDialog from '../components/UserDetailsDialog/UserDetailsDialog'

describe('UserDataDialog Component', () => {
    const user = {
        gender: "male",
        name: {
            title: "Mr",
            first: "مهراد",
            last: "پارسا"
        },
        location: {
            street: {
                number: 6781,
                name: "میدان آزادی"
            },
            city: "خوی",
            state: "خراسان شمالی",
            country: "Iran",
        },
        email: "mhrd.prs@example.com",
        dob: {
            date: "1972-05-17T17:06:57.512Z",
            age: 49
        },
        phone: "025-18942892",
        cell: "0969-922-5871",
        picture: {
            large: "https://randomuser.me/api/portraits/men/24.jpg",
            medium: "https://randomuser.me/api/portraits/med/men/24.jpg",
            thumbnail: "https://randomuser.me/api/portraits/thumb/men/24.jpg"
        },
    }


    it('Debe renderizar el componente', () => {
        render(<UserDataDialog onDialogClose={false} user={user}></UserDataDialog>)
    })
    it('Debe renderizar la palabra Ciudad dentro del dialog', () => {
        render(<UserDataDialog onDialogClose={false} user={user}></UserDataDialog>)
        expect(screen.queryByText(/Ciudad/i)).toBeInTheDocument()
    })
})
