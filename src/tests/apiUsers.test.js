import request from 'supertest'
import '@testing-library/jest-dom'

describe('Api Users', () => {
    it("Probando recibir array de usuarios", done => {
        request('https://randomuser.me/api')
        .get('/?results=100')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
    })
})



