# Configuracion del proyecto

Abrir terminal, navegar hasta el directorio donde se encuentra el proyecto y ejecutar el siguiente comando: 'npm install'


# Ejecución del proyecto

Abrir terminal, navegar hasta el directorio donde se encuentra el proyecto y ejecutar el siguiente comando: 'npm serve'


# Ejecución de testeos del proyecto

Abrir terminal, navegar hasta el directorio donde se encuentra el proyecto y ejecutar el siguiente comando: 'npm test a'


# Compilación del proyecto

Abrir terminal, navegar hasta el directorio donde se encuentra el proyecto y ejecutar el siguiente comando: 'npm build'



